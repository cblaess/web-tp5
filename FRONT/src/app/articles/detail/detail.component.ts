import { Component, OnInit, Input } from '@angular/core';
import { Article } from '../../models/article';
import { Store } from '@ngxs/store';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { AddArticle } from '../shared/article.action';
import { ArticleService } from 'src/app/service/article.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.sass']
})
export class DetailComponent implements OnInit {

  //@Input() article: Article;
  idArticle: number;
  article: Article;

  constructor(private store: Store, private articleService : ArticleService) {
    // on recupere l'id sur lequel on a clique + on recupere les informations sur celui ci à l'aide d'une requete http
    this.store.select(state => state.articles.detail).subscribe (u => this.idArticle = u);  
    this.articleService.getArticle(this.idArticle).subscribe(value => this.article = value[0]);
  }

  ngOnInit() {

  }

  ajouterAuPanier(article: Article)
  {
      this.addArticle (article);
  }

  addArticle(article: Article)
  {
    this.store.dispatch(new AddArticle(article));
  }
}
