import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthentificationService } from 'src/app/service/authentification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    loading = false;
    submitted = false;
    error = '';

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private authentificationService: AuthentificationService
    ) { 
        // redirect to home if already logged in
      //    if (this.authenticationService.currentUserValue) { 
      //      this.router.navigate(['/']);
      //  }
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;

        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        this.authentificationService.login(this.f.username.value, this.f.password.value).subscribe(
            res  => {
    
              if(res.Login)
              {
                localStorage.setItem('jwt_token', res.Authorization);
                localStorage.setItem('login', res.id);
                console.log(res.id);
                this.router.navigate(['/']);  
              }
              if(res.Erreur)
              {
                this.loading = false;
                this.error = res.Erreur;
              }
            }
          )
    }
}
