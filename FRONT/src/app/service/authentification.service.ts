import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { shareReplay, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})


export class AuthentificationService {
  
  constructor(private http: HttpClient) {
  }
    
  login(login:string, password:string ) {
    console.log(login);
    console.log(password);
      return this.http.post<any>(environment.url + '/api/users/login', {login, password, observe: 'response'});
  }

  register(nom : string, prenom : string, adresse : string, codePostal : number, ville : string, telephone : string, email : string,
    civilite : string, login : string, mdp : string, mdp2 : string) {
    return this.http.post<User>(environment.url + '/api/users/register', { nom, prenom, adresse, codePostal,ville, telephone, email,
      civilite, login, mdp, mdp2 });
    
  }

  getToken() {
    return localStorage.getItem('jwt_token');
  }

  get isLoggedIn(): boolean {
    let authToken = localStorage.getItem('jwt_token');
    return (authToken !== null) ? true : false;
  }
}
