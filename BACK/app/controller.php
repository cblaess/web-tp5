<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Firebase\JWT\JWT;

require '../vendor/autoload.php';

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Authorization');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');

$app = new \Slim\App();
$app->logins = array("corentin" => "blaess", "leo" => "shz");


const SECRET = "mykey123456789";
$jwt = new \Slim\Middleware\JwtAuthentication([
    "path" => "/api",
    "secure" => false,
    "passthrough" => ["/api/users/login","/api/users/register"],
    "secret" => JWT_SECRET,
    "attribute" => "decoded_token_data",
    "algorithm" => ["HS256"],
    "error" => function ($response, $arguments) {
        $data = array('ERREUR' => 'ERREUR', 'ERREUR' => 'AUTO');
        return $response->withHeader("Content-Type", "application/json")->getBody()->write(json_encode($data));
    }
]);

$app->add($jwt);

// retourne le logement avec l'id
$app->get('/api/logements/{id}', function (Request $request, Response $response, array $args){
    $json = file_get_contents("articles.json");
    $array = json_decode($json,1);
    $ID = $args['id'];
    $filteredData = [];
    foreach($array as $logement){
        if($logement['idLogement'] == $ID)
            $filteredData[] = $logement;
    }

    $obj = json_encode($filteredData);
    $response->getBody()->write("$obj");
    return $response;
});

// retourne la liste de tous les logements
$app->get('/api/logements', function (Request $request, Response $response, array $args) {
    $articles = file_get_contents("articles.json");
    echo $homepage;
    $response->getBody()->write("$articles");
    return $response;
});


// retourne si le login - mdp est valide ou non
$app->post('/api/users/login', function (Request $request, Response $response, array $args) use ($app){
        $body = $request->getParsedBody(); // Parse le body
        $searchLogin = $body['login'];
        $searchPwd = $body['password']; 
        
        // on regarde si le login est dans le dictionnaire, a remplacer par la liaison bdd par la suite
        foreach ($app->logins as $login=> $password) {
            $logged = ($searchLogin === $login && $searchPwd === $password) ? true : false;
            if($logged) break;
        }
        
        // le login et mdp est correct
        if($logged)
        {
            $issuedAt = time();
            $expirationTime = $issuedAt + 6000; // jwt valid for 60 seconds from the issued time
            $payload = array(
            // id a stocker lors de l'utilisation de la bdd
            'login' => $searchLogin,
            'iat' => $issuedAt,
            'exp' => $expirationTime
            );
            $token_jwt = JWT::encode($payload,JWT_SECRET, "HS256");
            
            $response = $response->withHeader("Authorization", "Bearer {$token_jwt}")->withHeader("Content-Type", "application/json");
            $data = array('Login' => 'Connexion reussi', "Authorization" => "Bearer {$token_jwt}", "id" => 1);

            return $response->withHeader("Content-Type", "application/json")->withJson($data);
        }
        else
        {
            $data = array('Erreur' => 'La combinaison Login / Mot de passe est incorrect');
            return $response->withJson($data);
        }
    });

// retourne si le login - mdp est valide ou non
$app->post('/api/users/register', function (Request $request, Response $response, array $args) use ($logins) {
    $body = $request->getParsedBody(); // Parse le body
    
    $donnees = array('nom' => $body['nom'], 
          'prenom' => $body['prenom'],
          'adresse' => $body['adresse'],
          'codePostal' => $body['codePostal'],
          'ville' => $body['ville'],
          'telephone' => $body['telephone'],
          'email' => $body['email'],
          'civilite' => $body['civilite'],
          'login' => $body['login']
    );

    return json_encode($donnees);
});

$app->run();